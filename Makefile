#
# Makefile
#

JC=javac
JR=jar
MK=make

SRCDIR=src
CLSDIR=classes
PKG=
SRC=$(SRCDIR)\Basic.java
CLS=$(CLSDIR)\Basic.class
DEP=$(SRC)

AllFiles: $(CLS)

$(CLS): $(DEP)
	@if not exist $(CLSDIR) mkdir $(CLSDIR)
	$(JC) -sourcepath $(SRCDIR) -d $(CLSDIR) $(SRC)
